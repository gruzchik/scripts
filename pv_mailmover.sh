#!/bin/bash

# author: ipogorelko@holbi.co.uk
#--
#From Plesk to Virtualmin. With same account name
#---
# You should run this script on the new(destination) server
#---



domain_mailfolder="/home/username/homes/"
domain="domain.com"
user="username"
remoteip="1.1.1.1"

/bin/ls -la ${domain_mailfolder} | grep ${domain} | awk {'print $9'}| while read line
do

# test using echo
#echo $line
#echo "rsync -e ssh -av --delete root@$remoteip:/var/qmail/mailnames/${domain}/${line}/Maildir/ ${domain_mailfolder}${line}/Maildir/"
#echo "chown -R ${line}@${domain}:${user} ${domain_mailfolder}${line}/Maildir/"

echo $line
rsync -e ssh -av --delete root@$remoteip:/var/qmail/mailnames/${domain}/${line}/Maildir/ ${domain_mailfolder}${line}/Maildir/
chown -R ${line}@${domain}:${user} ${domain_mailfolder}${line}/Maildir/


done
